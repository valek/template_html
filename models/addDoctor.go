package models

type Doctor struct {
	Id        string
	Lastname  string
	Otdelenie string
}

type DoctorProsmotr struct {
	Id        int
	Fio       string
	Otdelenie string
	Id_users  string
	Id_events string
	Id_ev     string
	Edate     string
	Note      string
}

func NewDoctorProsmotr(id int, fio, otdelenie, id_users, id_events, id_ev, edate, note string) *DoctorProsmotr {
	return &DoctorProsmotr{id, fio, otdelenie, id_users, id_events, id_ev, edate, note}
}

func NewDoctor(id, lastname, otdelenie string) *Doctor {
	return &Doctor{id, lastname, otdelenie}

}
