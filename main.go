package main

import (
	"database/sql"
	"fmt"
	"html/template"
	"net/http"
	"template_html/models"

	_ "github.com/go-sql-driver/mysql"
)

var (
	doctors         map[string]*models.Doctor
	doctorprosmotrs map[int]*models.DoctorProsmotr
)

func indexHandler(w http.ResponseWriter, r *http.Request) {

	t, err := template.ParseFiles("template/index.html", "template/header.html", "template/footer.html")
	if err != nil {
		fmt.Fprintf(w, err.Error())
	}

	db, err := sql.Open("mysql", "root:tz70xxkeq@/jobs?charset=utf8")
	if err != nil {
		panic(err)
	}

	rows, err := db.Query("select * from users u join general g on g.id_users = u.id join events e on e.id = g.id_events where g.id_users=1;")

	//rows, err := db.Query("SELECT id, fio FROM users")
	if err != nil {
		panic(err)
	}
	j := 0
	for rows.Next() {
		var (
			id        int
			fio       string
			otdelenie string
			id_users  string
			id_events string
			id_ev     string
			edate     string
			note      string
		)
		id = j
		j++
		fmt.Println(id)
		doctorprosmotr := models.NewDoctorProsmotr(id, fio, otdelenie, id_users, id_events, id_ev, edate, note)
		err = rows.Scan(&id, &doctorprosmotr.Fio, &doctorprosmotr.Otdelenie, &doctorprosmotr.Id_users, &doctorprosmotr.Id_events, &doctorprosmotr.Id_ev, &doctorprosmotr.Edate, &doctorprosmotr.Note)
		if err != nil {
			panic(err)
		}
		doctorprosmotrs[doctorprosmotr.Id] = doctorprosmotr
		//doctorprosmotrs[doctorprosmotr.Fio] = doctorprosmotr
		fmt.Println(doctorprosmotr)
	}
	t.ExecuteTemplate(w, "index", doctorprosmotrs)
}

func addDoctorHandler(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("template/addDoctor.html", "template/header.html", "template/footer.html")
	if err != nil {
		fmt.Fprintf(w, err.Error())
	}
	t.ExecuteTemplate(w, "addDoctor", nil)
}

func writeDoctorHandler(w http.ResponseWriter, r *http.Request) {
	id := r.FormValue("id")
	lastname := r.FormValue("lastname")
	otdelenie := r.FormValue("otdelenie")
	doctor := models.NewDoctor(id, lastname, otdelenie)

	doctors[doctor.Id] = doctor
	//fmt.Fprintf(w, doctor.Otdelenie)

	db, err := sql.Open("mysql", "root:tz70xxkeq@/jobs?charset=utf8")
	if err != nil {
		panic(err)
	}
	// Запрос в базу на добавление врача в базу данных (ФИО и Отделение)
	stmtIns, err := db.Prepare("INSERT INTO `users` (`id`, `fio`, `otdelenie`) VALUES (NULL, ?, ?)")
	if err != nil {
		panic(err.Error())
	}
	defer stmtIns.Close()

	// добавление врача в базу данных (ФИО и Отделение)
	res, err := stmtIns.Exec(doctor.Lastname, doctor.Otdelenie)
	if err != nil {
		panic(err)
	}

	uid, err := res.LastInsertId()
	if err != nil {
		panic(err)
	}
	//fmt.Fprintf(w, "Новый сотрудник добавлен", )
	http.Redirect(w, r, "/", 302)
	fmt.Println("Записали в базу: ", uid, doctor.Lastname, doctor.Otdelenie)

}

func main() {
	doctors = make(map[string]*models.Doctor, 0)
	doctorprosmotrs = make(map[int]*models.DoctorProsmotr, 0)

	http.Handle("/src/", http.StripPrefix("/src/", http.FileServer(http.Dir("./src/")))) //Указали папку с js и css
	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/addDoctor", addDoctorHandler)     //Форма добавления врача в расписание
	http.HandleFunc("/writeDoctor", writeDoctorHandler) //записываем врача в базу
	http.ListenAndServe(":3000", nil)
}
